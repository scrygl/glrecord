/////////////////////////////////////
#define time iTime*0.3
#define pi 3.14159265
#define deg pi/180.

mat2 r2d(float a) {
    return mat2(cos(a),sin(a),-sin(a),cos(a));
}

float cro(vec3 p, vec3 s) {
    return min(min(max(abs(p.x)-s.x,abs(p.z)-s.z),max(abs(p.x)-s.x,abs(p.y)-s.y)),max(abs(p.z)-s.z,abs(p.y)-s.y));
}

float box(vec3 p, vec3 s) {
    return max(abs(p.z)-s.z,max(abs(p.x)-s.x,abs(p.y)-s.y));
}
float bbox(vec3 p, vec3 s) {
    return max(box(p,s),-cro(p,s*0.5));
}

vec4 map(vec3 p) {
    p.z -= 3.2;
    p.xz *= r2d(sin(time*0.5));
    p.yz *= r2d(sin(time*0.75)*0.5);
    p.xz *= r2d(sin(time*0.7+p.y*3.)*0.6);
    p.yz *= r2d(sin(time*1.95+p.x*3.)*0.25);
    float s = 0.1;
    float bp = box(p,vec3(0.));
    for (int i=0;i<3;i++) {
        p = abs(p)-sin(time+bp*0.5)*0.1-0.1;
        //p.xz *= r2d(deg*90.);
    }
    p.xz *= r2d(sin(time*0.7));
    p.yz *= r2d(sin(time*1.95)*0.5);
    float d = abs(p.z-200.5);
    d = min(d,cro(p,vec3(0.05)));
    d = min(d,box(p,vec3(0.13)));
    d = min(d,bbox(p,vec3(0.25)));
    float ou = -box(p,vec3(10.));
    for (int i=0;i<5;i++) {
        ou = abs(ou)-0.02;
    }
    d = min(d,ou);
    return vec4(p,d);
}

vec2 RM(vec3 ro, vec3 rd, float side) {
    float dO = 0.;
    float ii = 0.;
    for (int i=0;i<299;i++) {
        vec3 p = ro+rd*dO;
        float dS = map(p).w*side;
        dO += dS;
        ii += 0.01;
        if (dO > 100. || dS < 0.002) {break;}
    }
    return vec2(dO,ii);
}

vec3 calcNorm(vec3 p) {
    vec2 h = vec2(0.002,0.);
    return normalize(vec3(
        map(p-h.xyy).w-map(p+h.xyy).w,
        map(p-h.yxy).w-map(p+h.yxy).w,
        map(p-h.yyx).w-map(p+h.yyx).w
    ));
}

vec3 colo(vec3 p, vec3 n, vec2 d) {
    vec3 col = n*0.4;
    col += 1.-d.x*0.2;
    if (d.x > 10.) {
        col *= 0.;
    }
    col += vec3(0.1,0.2,0.3)*0.2-d.x*0.2;
    return col;
}

void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
    vec2 uv = fragCoord.xy / iResolution.xy;
    vec2 R = iResolution.xy;
    float ar = R.x/R.y;
    uv -= 0.5;
    uv.x *= ar;
    vec3 col = vec3(0.);
    vec3 ro = vec3(0.);
    vec3 rd = normalize(vec3(uv,1.));
    vec2 d = RM(ro,rd,1.);
    vec3 p = ro+rd*d.x;
    vec3 n = calcNorm(p);
    float ior = 1.5;
    float rl = 0.99;
    col = colo(p,n,d);
    float od = d.x;
    float fd = 1.;
    for (int i=0;i<4;i++) {
        ro = p+n*0.004;
        rd = refract(rd,-n,1./ior);
        d = RM(ro,rd,-1.);
        p = ro+rd*d.x;
        n = calcNorm(p);
        col += colo(p,n,d)*rl;
        ro = p-n*0.003;
        rd = refract(rd,n,ior);
        //if (length(rd) == 0.) {rd = reflect(rd,n);}
        d = RM(ro,rd,1.);
        p = ro+rd*d.x;
        n = calcNorm(p);
        col += colo(p,n,d)*rl;
        rl *= rl;
        fd *= 1.8;
    }
    //col *= 1.-d.x*0.5;
    col -= od*0.5;
    col /= fd;
    
    //col *= 0.01;
    fragColor = vec4(col,1.0);
}
