//from https://stackoverflow.com/questions/15095909/from-rgb-to-hsv-in-opengl-glsl
// All components are in the range [0…1], including hue.
vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

 

// All components are in the range [0…1], including hue.
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}


//#define dtime time*hill(time,10.)
float bitm(vec2 uv,int c) {
    float h = 5.;
    float w = 3.;
    int p = int(pow(2.,w));
    float line1 = 9591.;
    uv = floor(vec2(uv.x*w,uv.y*h))/vec2(w,w);
    float c1 = 0.;
    float cc = uv.x + uv.y*w;
    c1 = mod( floor( float(c) / exp2(ceil(cc*w-0.6))) ,2.);
    c1 *= step(0.,uv.x)*step(0.,uv.y);
    c1 *= step(0.,(-uv.x+0.99))*step(0.,(-uv.y+1.6));
    return (c1);
}
 

vec3 slogo(vec2 uv, float ar, float size) {
    vec2 px = vec2(1./3.,1./5.);
    float ls = 4.1;
    uv.x = 0.993-uv.x;
    uv *= 8.*ls*(1./size);
    ls += 2.;
    float ul = length(uv);
    uv -= px.yx*0.5*0.5*ls;
    ul = length(vec2(uv.x*0.5,uv.y)-0.5);
    uv.x *= ar*1.75;
    int s = 29671;
    int c = 29263;
    int r = 31469;
    int y = 23186;
    uv.x= 5.-uv.x;
    float b = bitm(uv,s);
    uv.x -= 1./3.*4.;
    b += bitm(uv,c);
    uv.x -= 1./3.*4.;
    b += bitm(uv,r);
    uv.x -= 1./3.*4.;
    b += bitm(uv,y);
    float rr = step(0.,uv.x+px.x*13.)*step(0.,uv.y+px.y)*step(0.,(-uv.x+px.x*4.))*step(0.,(-uv.y+px.y*6.));
    b = clamp(b,0.,1.);
    //b = rr*floor(b);
    //float ptime = iTime;
    vec3 l = hsv2rgb(vec3(b+iTime/40.,0.1,rr-b*1.9))*rr;
    //l -= length(uv)*0.5;
    //l -= ul*rr*0.6;
    l -= 0.1-clamp(ul*0.1,rr*1.-b,0.1);
    //l -= 3.-ul*2;
    //l = clamp(l,-1.,1.);
    return vec3(l);
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = fragCoord/iResolution.xy;

    // Time varying pixel color
    vec3 col = texture(iChannel0,uv).rgb;

    // Output to screen
    col += slogo(uv,iResolution.x/iResolution.y,1.)/20.;
    fragColor = vec4(col,1.0);
}
