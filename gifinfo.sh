F=$(identify $1 | wc | cut -d" " -f 5)
R=$(identify -format '%wx%h,' $1 | cut -d, -f1)
D=$(printf "%0.1f\n" $(exiftool -Duration $1 | cut -d':' -f 2 | sed -e 's/^[[:space:]]*//'|cut -d' ' -f 1))
if [[ $D =~ ".0" ]]; then
    D=$(echo $D| bc -l | sed 's/.0\{1,\}$//')
fi
echo "$R GIF | $F Frames | $D Second loop"
